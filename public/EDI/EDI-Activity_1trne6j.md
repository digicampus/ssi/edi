# Get Wallet

<div class="story">
<div style="height:300px;width:1000px">
<img class="story" src="EDI/EDI-getwallet.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Holder |
| **Description** | Holder gets access to an EUDI compliant wallet |
| **Requirements** | It needs to be clearly defined in policies when a wallet is EUDI compliant and how this is ensured. |
| **Result** | Holder has access to a personal EUDI compliant wallet. |
| **Variations** | Wallet could be a mobile app on the holder’s phone or could hosted in the cloud. Other options are also possible. It needs to be clear how is determined which wallets are EUDI compliant. This can be done by a wallet issued by the public sector, commercial wallets or wallets issued in a hybrid way.  |
| **Riscs** | |
