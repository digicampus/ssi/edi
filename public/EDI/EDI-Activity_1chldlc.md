# Present selected QEAA’s to relying party

|  |  |
| ----------- | ----------- |
| **Roles** | Holder |
| **Description** | The wallet of the holder presents the requested QEAA’s to the relying party. |
| **Requirements** | |
| **Result** | The relying party has access to the presented QEAA's |
| **Variations** | There are multiple ways in which presented QEAA's are communicated to the service of the relying party. |
| **Riscs** | |
