# European Digital Identity (EDI) - proces

## About these diagrams
You can click on elements to get more information on these elements and / or navigate to subprocesses. 

This documentation is intended to help understand the impact of realizing the EDI, it's variations, riscs, gaps
and for instance it's relation to Self Sovereign Identity (SSI) on a high level. Technical details are omitted where possible.
Nothing here should be read as definitive and official as the EDI itself is work in progress.

These BPMN like diagrams are based on the European Digital Identity Architecture and Reference Framework Outline (published on 22-02-2022)
and ...
