# Share QEAA's

|  |  |
| ----------- | ----------- |
| **Roles** | Holder and relying party |
| **Description** | Share QEAA's with a relying party on request by the relying party. When the holder approves the request by the relying party the requested attested data in the form of on or more QEAA's is shared from an EUDI compliant wallet in control of the holder with the relying party. |
| **Requirements** | |
| **Result** | The relying party has possession of all the requested data and is assured of it being attested by one or more issuers accepted by the relying party |
| **Variations** | Multiple ways of sharing QEAA’s are possible depending on what devices the wallet in control of the holder and the QEAA receiving and verifying service in control of the relying party resides. We have described this process in a generic way. |
| **Riscs** | The relying party overasks the holder not properly taking into account the data minimalisation principle of the GDPR. This also includes the risk of overidentification. |
