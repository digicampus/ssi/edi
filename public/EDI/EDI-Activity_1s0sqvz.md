# Get QEAA

In this proces, the Holder gets one or more QEAA's issued to its wallet. Possibly in reference to a PID that is already held in the wallet when a QEAA contains
PII of a subject denoted by the PID to which the holder can prove he/she can be authenticated as being the subject (or authorized to act on behalf of).
