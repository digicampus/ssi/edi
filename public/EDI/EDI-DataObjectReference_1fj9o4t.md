# QEAA

|  |  |
| ----------- | ----------- |
| **Data Object** | |
| **Description** | QEAA's are data triples consisting of a subject, attribute name and attribute value, which are attested by a trusted issuing party (Issuer) through signing the triples. The subject can be empty but typically refers to other QEAA's such that QEAA's can form attestations of attestations. |
| **Requirements** | |
| **Assumptions** | |
| **Variations** | W3C Verifiable Credential data model or ...|
| **Riscs** | |
