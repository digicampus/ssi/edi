# Choose EDI compliant wallet to continue with

|  |  |
| ----------- | ----------- |
| **Roles** | Holder |
| **Description** | There are multiple EUDI compliant wallets that can present the QEAA’s requested by the relying party. The holder has to select an EUDI compliant wallet to be used for this. |
| **Requirements** | Holder has a EUDI compliant wallet installed which is thought to hold the requested QEAA’s.  |
| **Result** | The holder has selected an EUDI compliant wallet to present the requested QEAA’s with |
| **Variations** | Multiple solutions are possible to present the holder with a list of EUDI compliant wallets. For instance only the installed wallets or possible wallets the holder can install. At this point the holder can be informed about which wallet holds the information that has been requested or can be helped to obtain missing QEAA's |
| **Riscs** | Holder gets stuck not knowing what to do when there is no wallet installed to choose from and holder is not guided towards getting a wallet and obtaining the requested QEAA's. |
