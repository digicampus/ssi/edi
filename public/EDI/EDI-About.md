## About these diagrams

This documentation is intended to help understand the impact of realizing the European Digital Identity (EDI), it's variations, riscs, gaps
and for instance it's relation to Self Sovereign Identity (SSI) on a high level. Technical details are omitted where possible.
Nothing here should be read as definitive and official as the EDI itself is work in progress. If you have a gitlab account (and appropiate access)
you can see a list of issues, start a new issue (suggestion) and or even edit the documentation.

These BPMN like diagrams are based on the [European Digital Identity Architecture and Reference Framework Outline (published on 22-02-2022)](https://digital-strategy.ec.europa.eu/en/library/european-digital-identity-architecture-and-reference-framework-outline) (with "EDI ARF" we refer to this source)
and where appropriate on the [W3C Verifiable Credentials Data Model 1.1](https://www.w3.org/TR/vc-data-model/) (with "W3C" we refer to this source), [W3C Decentralized Identifiers (DIDs) v1.0](https://www.w3.org/TR/did-core/) and the [EBSI Architecture deck](https://ec.europa.eu/digital-building-blocks/wikis/download/attachments/447687044/%28210610%29%28EBSI_Architecture_Explained%29%28v1.02%29.pdf)

Both W3C standards around SSI as EDI describe a similair idea of Self Sovereign Identity in a broad sense. The same applies to EBSI which is much in line with the W3C. The naming of concepts and technical details (which for EDI has not been as established as like with W3C) do differ.

In these diagrams we use the following concept names for the same concepts in the W3C standard and EDI ARF outline:

| **Concept Name** | **Type** | **W3C** | **EDI** | **EBSI** | **comment** |
| ----------- | ----------- | ------------ | ----------- | ---------- | ---------- |
| **QEAA** | **data structure** | **Verifiable Credential** | **(Q)EAA** | **Verifiable Credential** | Where you read QEAA, it can also be an EAA depending on the issuer |
| **(EDI compliant) Wallet** | **infrastructure** | **Wallet** | EuDI Wallet | **(EBSI compliant) Wallet** | |
| **Holder** | **role** | **Holder** | End **Users** of EUDI Wallets | **Citizen (as Holder)** | The holder is the user of a wallet holding the QEAA |
| **Relying Party** | **role** | **Verifier** | **Relying Party** | **Verifier** | |
| **Issuer** | **role** | **Issuer** | **Qualified electronic attestation of attributes (QEAA) providers** | **Issuer** | |
| **Issuer** | **role** | **Issuer** | **Non-qualified electronic attestation of attributes (EAA) providers** | **Issuer** | |
| **Catalog** | **infrastructure** | **Verifiable Data Registry** | **Catalogue of attributes and schemes for the attestations of attribute providers** | **EBSI services and Trusted Registries (TIR/TSR/..)** | Note that with [compliance by design](https://dutchblockchaincoalition.org/en/use-cases-2/compliance-by-design), the set of (verifiably official) published laws could implicitly also (partly) form a schema registry |

The EDI ARF also describes PID providers, (Non-) Qualified certificate for electronic signature/seal providers (QCSP) and so on.
These are special types of issuers that may be authenticated through existing X509 based mechanisms (and which could be introduced into the W3C Framework
through a [X509 DID method](https://github.com/WebOfTrustInfo/rwot9-prague/blob/master/topics-and-advance-readings/X.509-DID-Method.md)). But the Wallet
itself on its own might form a QCSP too. Further the EDI ARF also mentions the concept of Wallet providers (to be able to denote what wallets are officially trusted).
