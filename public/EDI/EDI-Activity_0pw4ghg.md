# Get PID

The Holder can get a PID (Person Identification Data) issued to its wallet. Typically the PID is mainly a unique identifier that can stay secret within the wallet (only the Holder and possibly the party that issues the PID (Person Identification Data Providers) has knowledge of this PID). In a pure SSI solution, the holder can issue a PID him/herself, but it is often meaningless when not attested by a trusted party, or when additional PII in the form of QEAA's are issued in relation to it by such trusted parties.

Therefor a PID might also include one or more QEAA's with additional common PII: The EDI ARF mentions attributes on address, age, gender, civil status, family composition, nationality, education and training qualifications titles and licenses, professional qualifications titles and licenses, public permits and licenses, financial and company data.

Along with issuing a PID a Holder may be, and probably is, required to enroll to authentication for one or more levels of assuarance of as being the subject referred to by the QEAA's referring to the PID. Enrolling might involve the issuance of additional QEAA's with which the Holder can be Authenticated as the subject at a later moment.
