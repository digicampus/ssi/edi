# Authenticate holder as subject

|  |  |
| ----------- | ----------- |
| **Roles** | Holder |
| **Description** | A match is being made between the holder of the wallet and a PID attached to the wallet instance. A proof of this match will then be added to the requested QEAA’s |
| **Requirements** | There are QEAA’s requested that are related to a PID. The Holder has an EuDI compliant wallet installed capable of performing the match between the holder and the PID attached to the wallet instance at a proper level of assurance. The EuDI compliant app could use separate authentication app / services for this |
| **Result** | Proof of authentication |
| **Variations** | Multiple variations are possible, online or offline. At this point, the holder could be authenticated as being authorized to hold and share QEAA's about other people. |
| **Riscs** | Apart from over-identification, a risc is that the Holder has to authenticate many times for each QEAA which will result in a dissatisfying user experience. |
