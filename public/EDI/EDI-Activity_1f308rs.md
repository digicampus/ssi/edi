## Sharing QEAA's

<img src="EDI/EDI-toplevel.png" height="200">

|  |  |
| ----------- | ----------- |
| **Roles** | Holder and Relying Party |
| **Description** | The Holder (implicitly and optionally) requests a proof of identification of the relying party that shares the (proof of the) QEAA with the Holder. Then the Holder shreas the QEAA's requested by the relying party. Mutual authentication is ensured by doing this.  |
| **Requirements** | |
| **Result** | The relying party has access to the requested QEAA's |
| **Variations** | There are multiple ways in which presented QEAA's are communicated to the service of the relying party. |
| **Riscs** | |
