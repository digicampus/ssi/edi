# Request presentation of (selection of) QEAA's

|  |  |
| ----------- | ----------- |
| **Roles** | Relying party |
| **Description** | The relying party requests presentation of one or more QEAA’s. A request consists of reference to a publicly known attribute along with a list of issuers that attested this attribute and that are trusted by the relying party. |
| **Requirements** | Consensus over a harmonized attribute scheme. |
| **Result** | The holder has access to the information what QEAA’s are requested by the relying party and where to deliver them. |
| **Variations** | Communication for instance via multiple devices via QR or on one and the same device. |
| **Riscs** | The relying party overasks the holder not properly taking into account the data minimalisation principle of the GDPR. Also, the holder, if not yet had to be identified, can and should stay anonymous during this step. It is a risc implementations do not maintain anonimity.|
