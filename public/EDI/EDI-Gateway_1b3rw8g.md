## authentication required or not

At this point one of the following situations exist:

1. One or more QEAA's are requested that contain personally identifiable information (PII following the GDPR) and are tied (issued in relation) to a PID denoting the subject of these QEAA's and which is issued to the wallet instance that is selected by the Holder. In this case, and only when when the relying party requires some level of assurance as indicated in the request for these QEAA's, the Holder must present a proof of authentication of the subject of these QEAA's matching the required level of assurance along with presenting the QEAA's. For this, the Holder must authenticate as being the subject of these QEAA's  (see activity "Authenticate Holder as subject")

2. No QEAA's are requested for which authentication is required. Then the Holder does not have to authenticate.

3. A proof of authentication has already been presented very recently (as in a same session). The EuDI compliant wallet can just reuse the proof presented earlier in the session.

A variation to this is that the Holder is authorized to present QEAA's tied to a PID of another subject than him/her self. In this case the Holder must authenticate as being the one authorised to act on behalf of the subject. This authorisation is a QEAA on itself. 
