# Verify

|  |  |
| ----------- | ----------- |
| **Roles** | Relying party |
| **Description** | The relying party verifies the QEAA’s to be genuinely issued by issuers that are accepted by the relying party to provide assurance. |
| **Requirements** | |
| **Result** | The relying party has received the shared QEAA's and is assured about the validity of these shared QEAA's |
| **Variations** | |
| **Riscs** | |
