# PID (EuID)

A PID is a unique identifier that denotes a subject that QEAA's with personally identifiable information (PII) can refer to. Multiple QEAA's can be tied to a single subject this way.
A PID in itself is a QEAA and is issued (and may be tied to) the specific wallet instance presented by a holder at time of issuing the PID.
Along with a PID, additional QEAA's may be issued that hold typical PII and along with issuing a PID a Holder may be required to enroll to authentication for one or more levels of assuarance of as being the subject referred to by the QEAA's referring to the PID. Enrolling might involve the issuance of additional QEAA's with which the Holder can be Authenticated as the subject at a later moment.
