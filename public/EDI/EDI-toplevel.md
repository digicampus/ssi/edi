# European Digital Identity (EDI) - proces

If you are new here: [read about this documentation here first](?p=About&uc=EDI)

<div class="story">
<div style="height:300px;width:2000px">
<img class="story" src="EDI/EDI-getwallet.png" height="300">
<img class="story" src="RDW/RDW-toplevel.png" height="300">
</div></div>

### Top level idea
All participants handling QEAA's (Holders, Relying Party's, Issuers) get a wallet, optionally a PID (a QEAA in itself) and one or more QEAA's
to hold in their wallets. Then they can start sharing (proofs of) them. Any actor can be a holder, relying party or issuer when that makes sense.
Some actors are more qualified to act as a specific entity though.

Note that in this documentation we mostly show wallets as mobile app, but they also could be hosted (in control of the holder, issuer or relying party)
as web applications and or be secure applications on a PC.

### How to navigate
You can click on elements in the BPMN like diagram below to get more information on these elements and / or navigate to subprocesses.
For a decription of the concepts PID, QEAA and (EDI compliant) Wallet click on the corresponding items below.

The diagrams show happy flows only. At any activity, actors may choose to cancel, triggering an end event. Actors also can delete data from wallets
which is not elaborated on in these diagrams. You can also select a specific use case using the dropdown list to open (the same proces view of) a specific use case.
