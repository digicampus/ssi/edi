# Authorize sharing of requested QEAA’s with relying party

|  |  |
| ----------- | ----------- |
| **Roles** | Holder |
| **Description** | The holder is shown which QEAA’s (name and values) are about to be presented to the relying party and is asked whether to authorize this or not. Along with this, the Holder is informed of whether the service where the QEAA's are to be delivered to can be authenticated as the relying party |
| **Requirements** | |
| **Result** | Holder agrees to present the requested QEAA’s to the relying party. |
| **Variations** | Holder additionally receives information about which QEAA’s are typically requested by specific (type of) relying parties taking into account the data minimization principle of the GDPR. |
| **Riscs** | Holder does not have the information to assess whether or not it is justified that a relying party requests certain QEAA’s. For instance financial information for just a subscription for a magazine (over-questioning) Holder might blindly authorize all QEAA’s requested by the relying party. |
