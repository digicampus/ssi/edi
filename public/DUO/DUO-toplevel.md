# Example use case EDI - DUO online diploma proof

<img src="DUO/DUO-toplevel.png" height="200">

In this use case, within a online job application process,
an employer asks an applicant to present proof of education in the form of a electronic
equivalent of a diploma. We assume the applicant indicated to have the diploma in a EDI compliant wallet.
Note that it should always be possible for people to fall back on alternative methods
not requiring an electronic device.

Both the employer as the applicant need to have gotten a wallet, a PID and specific
QEAA's. In the case of the employer it would be an electronic equivalent of
a (proof of) a membership of the chamber of commerce. In the case of the applicant it would be an electronic equivalent of a diploma
all issued by appropiate authorities (through a QEAA provider, whether this is a QTSP
  on behalf of the appropiate authority or the appropiate authority itself). For instance,
  in the Netherlands, the diploma might be issued by an educator that is attested by DUO.

Then they may start sharing their QEAA's with each other to verify them.
