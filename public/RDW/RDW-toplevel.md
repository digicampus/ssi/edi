# Example use case EDI - RDW offline drivers license check

<div class="story">
<img class="story" src="RDW/RDW-toplevel.png" height="300">
</div>


|  |  |
| ----------- | ----------- |
| **Roles** | Driver and Police Officer |
| **Description** | Given the requirements are fulfilled: The Police Officer requests presentation of the Driver's driver license (with proof of identity of the driver as being the subject of the license). The Driver should request the police badge of the police officer (with proof the police officer is the subject of the badge).  Mutual authentication is ensured by doing this. If the driver can verify and is able to accept the badge, the driver presents the drivers license (with proofs). The police officer then can verify and accept/reject the license.  |
| **Requirements** | - Both police officer and driver should have access to a EDI compliant wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been issued by appropiate authorities (through a QEAA provider, whether this is a QTSP on behalf of the appropiate authority or the appropiate authority itself) - The wallets do not require an internet connection to function. |
| **Result** | The Driver is assured the police officer is officially an active police officer. The police officer is assured of the Driver having a valid drivers license.  |
| **Variations** | - The request for the drivers license could already contain the police badge with proofs (of that the police officer is the subject of the badge and authorized to ask for drivers licenses for instance) -  |
| **Riscs** | - Note that in this use case the police officer and/or driver might not have an active internet connection. In an offline situation, it can not be verified whether the badge or the license has been revoked. At best it can be checked the badge/license haven't been revoked up until last time a synchonisation of the wallets involved with a revocation register took place. - Note that it should always be possible for people to fall back on alternative methods not requiring an electronic device. |
| **Relevant regulation references** | |
