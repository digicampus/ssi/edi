## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-B3.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Driver |
| **Description** |  If the driver can verify and then accept or rejects the badge  |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** | The Driver is assured the police officer is officially an active police officer.  |
| **Variations** | Besides revocation of the badge it could also be checked whether the police officer is authorised to ask for drivers licenses |
| **Riscs** | In an offline situation, it can not be verified whether the badge has been revoked. At best it can be checked the badge haven't been revoked up until last time a synchonisation of the wallets involved with a revocation register took place. |
| **Relevant regulation references** | |
