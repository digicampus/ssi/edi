## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-A2.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Driver |
| **Description** | The driver presents the drivers license (with proofs). |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** | |
| **Variations** | |
| **Riscs** | |
| **Relevant regulation references** | |
