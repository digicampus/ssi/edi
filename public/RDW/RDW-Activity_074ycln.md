## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-B1.png" height="300">
<img class="story" src="RDW/RDW-B1b.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Driver |
| **Description** |  The Driver should request the police badge of the police officer (with proof the police officer is the subject of the badge). Mutual authentication is ensured by doing this. |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** | |
| **Variations** | The request for the drivers license could have also already contained the police badge with proofs so the driver does not need to ask for it.|
| **Riscs** | |
| **Relevant regulation references** | |
