## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-A3.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** |  Police Officer |
| **Description** |  The police officer verifies and either accepts  or rejects the license.  |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** | The police officer is assured of the Driver having a valid drivers license.  |
| **Variations** | |
| **Riscs** | In an offline situation, it can not be verified whether the license has been revoked. At best it can be checked the license haven't been revoked up until last time a synchronization of the wallet involved with a revocation register took place.|
| **Relevant regulation references** | |
