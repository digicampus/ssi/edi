## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-A1.png" height="300">
<img class="story" src="RDW/RDW-A1B12.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Police Officer |
| **Description** | The Police Officer requests presentation of the Driver's driver license (with proof of identity of the driver as being the subject of the license). |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** |  |
| **Variations** | The request for the drivers license could also contain the police badge with proofs so the driver does not need to ask for it. |
| **Riscs** | |
| **Relevant regulation references** | |
