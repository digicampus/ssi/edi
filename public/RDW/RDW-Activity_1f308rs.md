## Sharing QEAA's - Example use case EDI - RDW offline drivers license check

<div class="story">
<div style="height:300px;width:6000px">
<img class="story" src="RDW/RDW-A1b.png" height="300">
<img class="story" src="RDW/RDW-A1.png" height="300">
<img class="story" src="RDW/RDW-A1B12.png" height="300">
<img class="story" src="RDW/RDW-B1.png" height="300">
<img class="story" src="RDW/RDW-B1b.png" height="300">
<img class="story" src="RDW/RDW-B2.png" height="300">
<img class="story" src="RDW/RDW-B3.png" height="300">
<img class="story" src="RDW/RDW-A2.png" height="300">
<img class="story" src="RDW/RDW-A3.png" height="300">
</div></div>

|  |  |
| ----------- | ----------- |
| **Roles** | Driver and Police Officer |
| **Description** | The Police Officer requests presentation of the Driver's driver license (with proof of identity of the driver as being the subject of the license). The Driver should request the police badge of the police officer (with proof the police officer is the subject of the badge).  Mutual authentication is ensured by doing this. If the driver can verify and is able to accept the badge, the driver presents the drivers license (with proofs). The police officer then can verify and accept/reject the license.  |
| **Requirements** | Both police officer and driver should have access to a wallet instance in which their PID and involved QEAA's (police badge and drivers license) has been loaded and that does not require an internet connection to function. |
| **Result** | The Driver is assured the police officer is officially an active police officer. The police officer is assured of the Driver having a valid drivers license.  |
| **Variations** | The request for the drivers license could also contain the police badge with proofs so the driver does not need to ask for it.|
| **Riscs** | In an offline situation, it can not be verified whether the badge or the license has been revoked. At best it can be checked the badge/license haven't been revoked up until last time a synchonisation of the wallets involved with a revocation register took place.|
| **Relevant regulation references** | |
